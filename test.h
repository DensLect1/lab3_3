#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include "graph.h"
#include "test.h"

void test();
Graph<int> GenerateGraph();
void PrintGraphMatrice(Graph<int> gr);
Graph<int> InputGraph();
Graph<int> LoadGraphFromFile(std::string filename);


void test()
{
    std::cout << "Running tests!" << std::endl;

    Graph<int> gr(-1);

    int nodesCount = 10;

    for (int i = 0; i < nodesCount; i++) {
        Node nd = gr.AddNode();
        if (i > 0) {
            Arc<int> arc = gr.AddArc(i - 1, i, 1);
        }
    }
    //
    for (int i = 0; i < nodesCount; i++) {
        Node nd = gr.GetNode(i);
        std::cout << nd.GetId() << std::endl;
    }
    //
    std::vector <std::vector<int>> matrice = gr.GetMatrice();
    //
    for (int i = 0; i < matrice.size(); i++) {
        std::vector<int> row = matrice[i];
        std::string rowvalues = "";
        for (int j = 0; j < row.size(); j++) {
            //std::cout << row[j] << std::endl;
            rowvalues = rowvalues + std::to_string(row[j]) + ", ";
        }
        std::cout << rowvalues << std::endl;
    }
    std::cout << std::endl;
    // Создаем Граф по матрице смежности
    Graph<int> gr2(matrice, 0, -1);
    // Получаем новыю матрицу смежности. Должна быть идентична исходной.
    std::vector <std::vector<int>> matrice2 = gr2.GetMatrice();
    //
    std::cout << std::endl;
    for (int i = 0; i < gr2.arcs.size(); i++) {
        Arc<int> arc = gr2.arcs.find(i)->second;
        std::cout << std::to_string(arc.GetStartNode()) << "->" << std::to_string(arc.GetEndNode()) << "(" << std::to_string(arc.GetWeight()) << ")" << std::endl;
    }
    //
    std::cout << std::endl;
    for (int i = 0; i < matrice2.size(); i++) {
        std::vector<int> row = matrice2[i];
        std::string rowvalues = "";
        for (int j = 0; j < row.size(); j++) {
            //std::cout << row[j] << std::endl;
            rowvalues = rowvalues + std::to_string(row[j]) + ", ";
        }
        std::cout << rowvalues << std::endl;
    }
}

Graph<int> GenerateGraph()
{
    int size = 5;
    // создаем матрицк смежности
    std::vector <std::vector<int>> matrice;
    // Заполняем нулями
    for (int i = 0; i < size; i++) {
        std::vector<int> row;
        for (int j = 0; j < size; j++) {
            row.push_back(0);
        }
        matrice.push_back(row);
    }
    // Заполняем связи
    // граф по умолчанию
    matrice[0][0] = 0; matrice[0][1] = 2;   matrice[0][2] = 0;  matrice[0][3] = 3;  matrice[0][4] = 0;
    matrice[1][0] = 2; matrice[1][1] = 0;   matrice[1][2] = 4;  matrice[1][3] = 4;  matrice[1][4] = 0;
    matrice[2][0] = 0; matrice[2][1] = 4;   matrice[2][2] = 0;  matrice[2][3] = 5;  matrice[2][4] = 6;
    matrice[3][0] = 3; matrice[3][1] = 4;   matrice[3][2] = 5;  matrice[3][3] = 0;  matrice[3][4] = 0;
    matrice[4][0] = 0; matrice[4][1] = 0;   matrice[4][2] = 6;  matrice[4][3] = 0;  matrice[4][4] = 0;
    //
    Graph<int> grTest(matrice, 0, -1);
    //
    return grTest;
    //
}

void PrintGraphMatrice(Graph<int> gr) {
    //
    std::vector <std::vector<int>> matrice = gr.GetMatrice();
    //
    for (int i = 0; i < matrice.size(); i++) {
        std::vector<int> row = matrice[i];
        std::string rowvalues = "";
        for (int j = 0; j < row.size(); j++) {
            //std::cout << row[j] << std::endl;
            rowvalues = rowvalues + std::to_string(row[j]) + ", ";
        }
        std::cout << rowvalues << std::endl;
    }
}

Graph<int> InputGraph()
{

    int size = -1;
    while (size <= 0) {
        std::cout << "Enter grap nodes count: ";
        std::cin >> size;
        if (size <= 0) {
            std::cout << "... Grap nodes count should be greater than 0! Please try again.";
        }
    }
    // создаем матрицу смежности
    std::vector <std::vector<int>> matrice;
    // Заполняем нулями
    for (int i = 0; i < size; i++) {
        std::vector<int> row;
        for (int j = 0; j < size; j++) {
            std::cout << "Enter element [" << i << "][" << j << "]: ";
            int number = -1;
            while (number < 0) {
                std::cin >> number;
                if (number < 0) {
                    std::cout << "... Enter 0, if nodes not connected, otherwise enter int weight!";
                }
            }
            row.push_back(number);
        }
        matrice.push_back(row);
    }
    //
    Graph<int> grInput(matrice, 0, -1);
    //
    return grInput;
    //
}

bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

Graph<int> LoadGraphFromFile(std::string filename) {
    std::vector <std::vector<int>> matrice;
    // Считываем из файла:
    std::ifstream file(filename);
    std::string str;
    int line_number = 0;
    while (std::getline(file, str))
    {
        // Удаляем все пробелы
        str.erase(std::remove_if(str.begin(), str.end(), isspace), str.end());
        int start_segment = 0;
        int end_segment = 0;
        std::string str_number;
        std::vector<int> row;
        for (int i = 0; i < str.length(); i++) {
            if (str[i] != ',') {
                end_segment++;
            }
            else {
                str_number = str.substr(start_segment, end_segment - start_segment);
                if (is_number(str_number)) {
                    row.push_back(stoi(str_number));
                }
                else {
                    std::cout << "Invalid number [" << str_number << "] at line " << line_number << std::endl;
                    row.push_back(0);
                }
                end_segment++; 
                start_segment = end_segment;
            }
        }
        if (end_segment > start_segment) {
            // последнее число
            str_number = str.substr(start_segment, end_segment - start_segment);
            if (is_number(str_number)) {
                row.push_back(stoi(str_number));
            }
            else {
                std::cout << "Invalid number [" << str_number << "] at line " << line_number << std::endl;
                row.push_back(0);
            }
        }
        line_number++;
        matrice.push_back(row);
    }
    
    return Graph<int>(matrice, 0, -1);
    
}

#endif
