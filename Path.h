#ifndef PATH_H
#define PATH_H

#include <iostream>
#include <set>
#include <vector>
#include "graph.h"

using namespace std;

class Path {
private:
    vector<Node> path;                 
    int length;                        

public:
    Path(Node start);
    Path& operator= (const Path& newpath);
    vector<Node> GetPath() const;       
    int GetLength() const;             
    void AddNode(Node node, int weight);
    void AddPath(Path added_path); 
    Node GetLastNode(); 

};

ostream& operator<<(ostream& os, const Path& p);


Path::Path(Node start) {
    path.push_back(start);
    length = 0;
}

Path& Path::operator=(const Path& newpath) {
    path = newpath.GetPath();
    length = newpath.GetLength();
    return *this;
}

vector<Node> Path::GetPath() const { return path; }

int Path::GetLength() const { return length; }

void Path::AddNode(Node node, int weight) {
    path.push_back(node);
    length += weight;
}

void Path::AddPath(Path added_path) {
    for (int i = 0; i < added_path.GetPath().size(); ++i) {
        Node last = path[path.size() - 1];
        Node added_node = added_path.GetPath()[i];
        if (last.GetId() != added_node.GetId()) {
            path.push_back(added_node);
        }
    }
    length += added_path.GetLength();
}

ostream& operator<<(ostream& os, const Path& p) {
    size_t n = p.GetPath().size();
    for (size_t i = 0; i < n; i++) {
        os << p.GetPath()[i].GetId() + 1;
        if (i != n - 1) {
            os << "->";
        }
        else {
            os << " (";
        }
    }
    os << p.GetLength() << ')' << endl;
    return os;
}

Node Path::GetLastNode() {
    return path[path.size() - 1];
}


#endif
