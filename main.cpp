#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "graph.h"
#include "test.h"
#include "salesman.h"

int main()
{
    Graph<int> grTest = LoadGraphFromFile("graph2.txt");

    salesman sm(grTest);
    std::cout << std::endl;
    Path res = sm.calc({ 2, 6 }, grTest.GetNode(0));
    std::cout << "Result: " << res;
}
