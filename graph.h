#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <vector>
#include <iostream>

using namespace std;

template <typename Tweight> 
// максимизация прибыли, минимизация времени, 
// минимизация пути, максимизация полученный прибыли к затраченому времени
struct Arc {
private:
    int id;
    int start_node;
    int end_node;
    Tweight weight;

public:
    Arc(int id, int start, int end, Tweight weight) {
        this->id = id;
        this->start_node = start;
        this->end_node = end;
        this->weight = weight;
    }

    int GetId() {
        return this->id;
    }
    int SetId(int new_id) {
        this->id = new_id;
        return 0;
    }

    int GetStartNode() {
        return this->start_node;
    }
    int SetStartNode(int start) {
        this->start_node = start;
        return 0;
    }

    int GetEndNode() {
        return this->end_node;
    }
    int SetEndNode(int end) {
        this->end_node = end;
        return 0;
    }

    Tweight GetWeight() {
        return this->weight;
    }
    int SetWeight(Tweight new_weight) {
        this->weight = new_weight;
        return 0;
    }
};


struct Node {
private:
    int id;
    int value;
    vector <int> out_arc;

public:
    Node(int id_)
        : id(id_)
        , out_arc() {
        value = 0;
    }

    Node(const Node* other_)
        : id(other_->GetId())
        , value(other_->GetValue())
        , out_arc(other_->GetOutArcs()) {
    }


    int GetId() const {
        return this->id;
    }
    int SetId(int new_id) {
        this->id = new_id;
        return 0;
    }

    int GetValue() const {
        return this->value;
    }
    int SetValue(int new_value) {
        this->value = new_value;
        return 0;
    }

    vector<int> GetOutArcs() const {
        return out_arc;
    }
    int SetOutArcs(vector<int> out_) {
        this->out_arc = out_;
        return 0;
    }
};

template <typename Tweight>
struct Graph {
public:
    // inline static int id_arc = 0;
    // inline static int id_node = 0;
    int id_arc = 0;
    int id_node = 0;
    
    map <int, struct Arc<Tweight>> arcs;
    vector<Node> nodes;
    Tweight invalid_weight;

    Graph()
        : arcs()
        , nodes() {}

    Graph(Tweight graph_invalid_weight)
        : arcs()
        , nodes() {
        invalid_weight = graph_invalid_weight;
    }

    // Создаем граф по матрице смежности
    Graph(std::vector <std::vector<Tweight>> matrice, Tweight zero_weight, Tweight graph_invalid_weight)
        : arcs()
        , nodes() {

        invalid_weight = graph_invalid_weight;

        for (int i = 0; i < matrice.size(); i++) {
            Node node = AddNode();
        }

        for (int i = 0; i < matrice.size(); i++) {
            std::vector<Tweight> row = matrice[i];
            Node node = GetNode(i);
            for (int j = 0; j < row.size(); j++) {
                if (row[j] != zero_weight) {
                    // Добавляем связь
                    Arc<Tweight> arc = AddArc(i, j, row[j]);
                }
            }
        }
    }

    Node AddNode() {
        Node new_(id_node++);
        nodes.push_back(new_);
        return new_;
    }

    Arc<Tweight> AddArc(int start, int end, Tweight weight) {
        // Проверяем, что start имеет валидный номер узла
        if (start < 0 || start >= nodes.size()) {
            return GetInvalidArc();
        }
        // Проверяем, что end имеет валидный номер узла
        if (end < 0 || end >= nodes.size()) {
            return GetInvalidArc();
        }
        Arc<Tweight> arc(id_arc, start, end, weight);
        vector<int> tmp = nodes[start].GetOutArcs();
        tmp.push_back(id_arc);
        (nodes[start]).SetOutArcs(tmp);
        arcs.insert({ id_arc, arc });
        id_arc++;
        //arcs[id_arc++] = arc;
        //
        return arc;
    }
    
    Arc<Tweight> GetInvalidArc() {
        return Arc<Tweight>(-1, -1, -1, invalid_weight);
    }

    Node GetNode(int id) {
        if (id < 0 || id >= nodes.size()) {
            return Node(-1);
        }
        else {
            return nodes[id];
        }
    }

    size_t GetNodesCount() {
        return nodes.size();
    }

    Arc<Tweight> GetArc(int id) {
        if (id < 0 || id >= arcs.size()) {
            // Возвращаем ошибочный инстанс
            return GetInvalidArc();
        }
        else {
            return arcs.find(id)->second;
        }
    }

    vector <Node> GetConnectedNodes(int id) {
        vector <Node> connectedNodes;
        if (id < 0 || id >= nodes.size()) {
            return connectedNodes;
        }
        else {
            Node node = nodes[id];
            // Проходим по всем исходящим дугам
            for (int i = 0; i < node.out_arc.size(); i++) {
                Arc<Tweight> arc = GetArc(node.out_arc[i]);
                if (arc.end_node >= 0 && arc.end_node < nodes.size()) {
                    connectedNodes.push_back(GetNode(arc.end_node));
                }
            }
            return connectedNodes;
        }
    }

    vector <vector<Tweight>> GetMatrice() {
        vector <vector<Tweight>> matrice;
        // Инициализируем матрицу (все нули)
        for (int i = 0; i < nodes.size(); i++) {
            vector<int> row;
            for (int j = 0; j < nodes.size(); j++) {
                row.push_back(0);
            }
            matrice.push_back(row);
        }
        // Заполняем связи единицами
        for (int i = 0; i < nodes.size(); i++) {
            Node node = GetNode(i);
            for (int j = 0; j < node.GetOutArcs().size(); j++) {
                Arc<Tweight> arc = GetArc(node.GetOutArcs()[j]);
                if (arc.GetEndNode() >= 0) {
                    matrice[i][arc.GetEndNode()] = arc.GetWeight();
                }
            }
        }
        return matrice;
    }
};

#endif
