#ifndef SALESMAN_H
#define SALESMAN_H

#include <iostream>
#include <limits>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include "graph.h"
#include "Path.h"

using namespace std;

class salesman { // переименовать (промежуточный этап)
private:
    Graph<int> cgraph;                                
    vector<int> vectorP;                         
    vector<bool> available_nodes;                
    int GetNextAvailableNode();

public:
    salesman();
    salesman(Graph<int> gr);
    void SetGraph(Graph<int> gr) {
        cgraph = gr;
    }
    Path* Dijkstra(Node node, Path* data);         
    Path calc(vector<int> v, Node start);        
    void print_vector(vector<int> vector_to_ptint);
};


salesman::salesman() {}
salesman::salesman(Graph<int> gr) {
    cgraph = gr;
}


int salesman::GetNextAvailableNode() {
    int selected_node_id = -1;
    int min_val = numeric_limits<int>::max();
    for (int i = 0; i < cgraph.nodes.size(); i++) {
        if (available_nodes[i] && cgraph.nodes[i].GetValue() <= min_val) {
            selected_node_id = i;
            min_val = cgraph.nodes[i].GetValue();
        }
    }
    return selected_node_id;
}


Path* salesman::Dijkstra(Node node, Path* data) {
    Graph<int> gr(-1);
    size_t nodes_count = cgraph.GetNodesCount();
    vector <vector<int>> matrice = cgraph.GetMatrice();

    data = (Path*)calloc(nodes_count, sizeof(Path));      
    vector<int> distances;                           
    vector<int> out;                                
    distances.resize(nodes_count);

    out.resize(nodes_count);
    for (int i = 0; i < nodes_count; ++i) {
        if (i == node.GetId()) {
            distances[i] = 0;
            data[i] = Path(node);
        }
        else {
            distances[i] = numeric_limits<int>::max();
        }
        out[i] = 0;
    }
    int min = numeric_limits<int>::max();
    int index = -1;
    do {
        min = numeric_limits<int>::max();
        for (int i = 0; i < nodes_count; ++i) {
            if ((out[i] == 0) && (distances[i] < min)) {
                min = distances[i];
                index = i;
            }
        }
        if (min != numeric_limits<int>::max()) {
            for (int i = 0; i < nodes_count; ++i) {
                if (matrice[index][i] > 0) {
                    int newdistance = min + matrice[index][i];
                    if (newdistance < distances[i]) {
                        distances[i] = newdistance;
                        // Оптимальный путь до вершины
                        data[i] = data[index];
                        data[i].AddNode(i, matrice[index][i]);
                    }
                }
            }
            out[index] = 1;
        }
    } while (min < numeric_limits<int>::max());
    return data;
}

// вычисление оптимального пути
Path salesman::calc(vector<int> nodes_to_visit, Node start) {
    map<int, Path*> distances;
    Path* current_path = (Path*)calloc(cgraph.GetNodesCount(), sizeof(Path));
    current_path = Dijkstra(start, current_path);
    distances[start.GetId()] = current_path;
    std::cout << "Dijkstra(" << start.GetId() + 1 << ")" << std::endl;
    for (int i = 0; i < cgraph.GetNodesCount(); i++) {
        std::cout << "  " << i + 1 << ". " << current_path[i];
    }
    for (size_t i = 0; i < nodes_to_visit.size(); ++i) {
        current_path = Dijkstra(nodes_to_visit[i], current_path);
        distances[nodes_to_visit[i]] = current_path;
    }
    Path best_path = Path(start); 
    sort(nodes_to_visit.begin(), nodes_to_visit.end());
    for (int i = 0; i < nodes_to_visit.size(); ++i) {
        Node last = best_path.GetLastNode();
        best_path.AddPath(distances[last.GetId()][nodes_to_visit[i]]);
    }

    Node last = best_path.GetLastNode();
    best_path.AddPath(distances[last.GetId()][start.GetId()]);
    print_vector(nodes_to_visit);
    std::cout << ": " << best_path;
    next_permutation(nodes_to_visit.begin(), nodes_to_visit.end());
    do {
        Path current_path = Path(start); 
        for (size_t i = 0; i < nodes_to_visit.size(); ++i) {
            Node last = current_path.GetLastNode();
            current_path.AddPath(distances[last.GetId()][nodes_to_visit[i]]);
            if (current_path.GetLength() > best_path.GetLength())
                break;
        }
        Node last = current_path.GetLastNode();
        current_path.AddPath(distances[last.GetId()][start.GetId()]);
        if (current_path.GetLength() < best_path.GetLength()) {
            best_path = current_path;
        }
        print_vector(nodes_to_visit);
        cout << ": " << current_path;
    } while (next_permutation(nodes_to_visit.begin(), nodes_to_visit.end()));

    return best_path;
}


void salesman::print_vector(vector<int> vector_to_ptint) {
    cout << "[";
    for (int i = 0; i < vector_to_ptint.size(); i++) {
        cout << vector_to_ptint[i] + 1;
        if (i < vector_to_ptint.size() - 1) {
            cout << ", ";
        }
    }
    cout << "]";
}

#endif

